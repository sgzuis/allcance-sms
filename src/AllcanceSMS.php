<?php

namespace SGzuis;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use stdClass;

class AllcanceSMS
{
    private string $url = 'https://corporativo.allcancesms.com.br';
    private string $urn = '/app/modulo/api/index.php';

    private string $username;
    private string $password;

    private Client $httpClient;

    /**
     * @param string $username
     * @param string $password
     *
     * @return void
     *
     * @throws GuzzleException
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;

        $this->httpClient = new Client([
            'base_uri' => $this->url,
        ]);
    }

    /**
     * @return string
     *
     * @throws GuzzleException
     */
    public function login(): string
    {
        $request = $this->httpClient->request('GET', $this->urn, [
            'headers' => [
                ...$this->defaultHeaders(),
            ],
            'query' => [
                ...$this->defaultParams(),
                'action' => 'login',
            ]
        ]);

        return $request->getBody()->getContents();
    }

    /**
     * @return array
     */
    private function defaultHeaders(): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * @return array
     */
    private function defaultParams(): array
    {
        return [
            'lgn' => $this->username,
            'pwd' => $this->password,
        ];
    }

    /**
     * @param string $subject
     * @param string $message
     * @param string|array $phones
     *
     * @return stdClass
     *
     * @throws GuzzleException
     * @throws JsonException
     */
    public function send(string $subject, string $message, string|array $phones): stdClass
    {
        $phones = !is_array($phones) ? [$phones] : $phones;

        foreach ($phones as $key => $phone) {
            $phones[$key] = substr(preg_replace('/\D/', '', $phone), -11, 11);
        }

        $request = $this->httpClient->request('GET', $this->urn, [
            'headers' => [
                ...$this->defaultHeaders(),
            ],
            'query' => [
                ...$this->defaultParams(),
                'action' => 'sendsms',
                'subject' => $subject,
                'msg' => $message,
                'numbers' => implode(',', $phones),
            ]
        ]);

        return json_decode($request->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $ids
     *
     * @return stdClass
     *
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getCampaign(string $ids): stdClass
    {
        $request = $this->httpClient->request('GET', $this->urn, [
            'headers' => [
                ...$this->defaultHeaders(),
            ],
            'query' => [
                ...$this->defaultParams(),
                'action' => 'GetCampanha',
                'idCamp' => $ids,
            ]
        ]);

        return json_decode($request->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @return stdClass
     *
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getInbox(): stdClass
    {
        $request = $this->httpClient->request('GET', $this->urn, [
            'headers' => [
                ...$this->defaultHeaders(),
            ],
            'query' => [
                ...$this->defaultParams(),
                'action' => 'GetResposta',
            ]
        ]);

        return json_decode($request->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
    }
}